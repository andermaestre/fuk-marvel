import React from 'react';
import './App.css';
import md5 from 'md5';
import {PUBLIC_KEY,PRIVATE_KEY,API_URL} from './marvelApiKey'

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      resCode: 'pitter'

    }
    this.callAppi();
  }

  callAppi() {
    const ts = Math.random().toString(36).substring(7);
    const hash = md5(ts + PRIVATE_KEY + PUBLIC_KEY);
    //const fullUrl = `http://gateway.marvel.com/v1/public/comics?ts=${ts}&apikey=${PUBLIC_KEY}&hash=${hash}&limit=100`
    const fullUrl = `https://gateway.marvel.com:443/v1/public/characters/?ts=${ts}&apikey=${PUBLIC_KEY}&hash=${hash}&limit=100`
    fetch(fullUrl)
    .then((response) => {
      return response.json()
    })
    .then((recurso) => {
      console.log(recurso)
      this.setState({resCode: JSON.stringify(recurso)});
    })
  }

  render() {
    const { resCode } = this.state;
    return (<h1>{resCode}</h1>);
  }
}


export default App;
